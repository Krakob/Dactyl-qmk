#!/usr/bin/env sh

# Safety first
[ "$1" = "" ] && echo "No hex file given." && exit 1
[ ! -f "$1" ] && echo "Given hex file $1 does not exist" && exit 1

# Clean-up
rm /tmp/flash1 /tmp/flash2

ls /dev/tty* > /tmp/flash1
printf "Detecting USB port, reset your controller now."
while [ -z $USB ]; do
  sleep 1
  printf "."
  ls /dev/tty* > /tmp/flash2
  USB=`diff /tmp/flash1 /tmp/flash2 | grep -o '/dev/tty.*'`
done
echo ""
echo "Detected controller on USB port at $USB"
sleep 1
avrdude -p atmega32u4 -c avr109 -P $USB -U flash:w:$1
